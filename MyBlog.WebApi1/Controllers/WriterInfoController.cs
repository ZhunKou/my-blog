﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyBlog.IService;
using MyBlog.Model;
using MyBlog.Model.DTO;
using MyBlog.WebApi1.Utility._MD5;
using MyBlog.WebApi1.Utility.ApiResult;
using System;
using System.Threading.Tasks;

namespace MyBlog.WebApi1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class WriterInfoController : ControllerBase
    {
        private readonly IWriterInfoService _iWriterInfoService;
        public WriterInfoController(IWriterInfoService iWriterInfoService)
        {
            this._iWriterInfoService = iWriterInfoService;
        }
        [HttpPost(template:"Create")]
        public async Task<ApiResult> Create(string name, string username,string userpwd) 
        {
            //数据校验
            WriterInfo writerInfo = new WriterInfo
            {
                Name = name,
                //加密密码
                UserName = username,

                UserPwd = MD5Helper.MD5Encrypt32(userpwd)
            };
            //判断数据库中是否已经存在账号跟要添加的账号相同的数据
             var oldwriter= await _iWriterInfoService.FindAsync(c=>c.UserName==username);
            if (oldwriter != null) return ApiResultHelper.Error(msg: "账号已经存在");
            bool b=await _iWriterInfoService.CreateAsync(writerInfo);
            if (!b) return ApiResultHelper.Error(msg: "添加失败");
            return ApiResultHelper.Success(writerInfo);
        }
        [HttpPut(template: "Edit")]
        public async Task<ApiResult> Edit(string name)
        {
            int id=Convert.ToInt32(this.User.FindFirst(type:"Id").Value);
            var wrier= await _iWriterInfoService.FindAsync(id);
            wrier.Name=name;
            bool b= await _iWriterInfoService.EditAsync(wrier);
            if (!b) return ApiResultHelper.Error(msg: "修改失败");
            return ApiResultHelper.Error(msg: "修改成功");
        }
        [AllowAnonymous]
        [HttpGet(template: "FindWriter")]
        public async Task<ApiResult> FindWriter([FromServices]IMapper imapper, int Id)
        {
            var wrier= await _iWriterInfoService.FindAsync(Id);
            var writerDTO= imapper.Map<WriterDTO>(wrier);
            return ApiResultHelper.Success(writerDTO);
        }
        }
}
