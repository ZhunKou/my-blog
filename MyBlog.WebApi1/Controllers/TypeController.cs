﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyBlog.IService;
using MyBlog.Model;
using MyBlog.WebApi1.Utility.ApiResult;
using System.Threading.Tasks;

namespace MyBlog.WebApi1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class TypeController : ControllerBase
    {
        private readonly ITypeInfoService _itypeInfoService;
        public TypeController(ITypeInfoService iTypeInfoService)
        {
            this._itypeInfoService = iTypeInfoService;
        }
        [HttpGet(template: "Get")]
        public async Task<ApiResult> Types()
        {
            var type = await _itypeInfoService.QueryAsync();
            if (type.Count==0) return ApiResultHelper.Error(msg: "没有更多的类型");
            return ApiResultHelper.Success(type);
        }
        [HttpPost(template: "Create")]
        public async Task<ApiResult> Create(string name)
        {
            #region 数据验证】
            if (string.IsNullOrWhiteSpace(name)) return ApiResultHelper.Error(msg: "类型名不能为空");
            #endregion
            TypeInfo type = new TypeInfo
            {
                Name = name
            };
            bool b = await _itypeInfoService.CreateAsync(type);
            if (!b) return ApiResultHelper.Error(msg: "添加失败");
            return ApiResultHelper.Success(b);
        }
        [HttpPut(template: "Edit")]
        public async Task<ApiResult> Edit(int id, string name)
        {
            var type = await _itypeInfoService.FindAsync(id);
            if (type == null) return ApiResultHelper.Error(msg: "没有找到该文章类型");
            type.Name = name;
            bool b = await _itypeInfoService.EditAsync(type);
            if (!b) return ApiResultHelper.Error(msg: "修改失败");
            return ApiResultHelper.Success(type);
        }
        [HttpDelete(template: "Delete")]
        public async Task<ApiResult> Delete(int id)
        {
            bool b = await _itypeInfoService.DeleteAsync(id);
            if (!b) return ApiResultHelper.Error(msg: "修改失败");
            return ApiResultHelper.Success(b);
        }
    }
}
