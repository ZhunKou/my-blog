﻿using AutoMapper;
using MyBlog.Model;
using MyBlog.Model.DTO;

namespace MyBlog.WebApi1.Utility.AutoMapper
{
    public class CustomAutoMapperProfile:Profile
    {
        public CustomAutoMapperProfile() 
        {
            base.CreateMap<WriterInfo,WriterDTO>();
            base.CreateMap<BlogNews, BlogNewsDTO>()
            .ForMember(dest => dest.TypeName, soure => soure.MapFrom(src => src.TypeInfo.Name))
            .ForMember(dest => dest.WriterName, soure => soure.MapFrom(src => src.WriterInfo.Name));
        }
    }
}
