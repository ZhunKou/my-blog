﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MyBlog.IService;
using MyBlog.JWT.Utility._MD5;
using MyBlog.JWT.Utility.ApiResult;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MyBlog.JWT.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class AuthoizeController : ControllerBase
    {
        private readonly IWriterInfoService _writerInfoService;
        public AuthoizeController(IWriterInfoService writerInfoService) 
        {
            this._writerInfoService = writerInfoService;
        }

        [HttpPost(template:"Login")]
        public async Task<ApiResult> Login(string username, string password) 
        {
            string pwd=MD5Helper.MD5Encrypt32(password);
            //数据校验
            var a= await _writerInfoService.FindAsync(c=>c.UserName==username&&c.UserPwd== pwd);
            if (a != null) 
            {
                //登录成功
                var claime = new Claim[]
                {
                    new Claim(ClaimTypes.Name,a.Name),
                    new Claim(type:"Id",a.Id.ToString()),
                    new Claim(type:"UserName",a.Name.ToString())
                    
                };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(s: "SDMC-CJAS1-SAD-DFSFA-SADHJVF-VF"));
                //Iissuer代表颁发Token的Web应用程序， audience是Token的受理者
                var token = new JwtSecurityToken
                    (
                        issuer: "http://localhost:6060",
                        audience: "http://localhost:5000",
                        claims: claime,
                        notBefore: System.DateTime.Now,
                        expires: System.DateTime.Now.AddHours(1),
                        signingCredentials : new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
                    );
                var jwtToken=new JwtSecurityTokenHandler().WriteToken(token);
                return ApiResultHelper.Success(jwtToken);
            }
            else
            {
                return ApiResultHelper.Error(msg: "账号或密码错误");
            }
        
        }
    }
}
